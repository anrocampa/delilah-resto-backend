FROM node:16

WORKDIR /usr/src/app

COPY package*.json ./

RUN npm install

COPY . .

EXPOSE 3000

# Add wait-for-it
RUN chmod +x wait-for-it.sh

CMD ["./wait-for-it.sh" , "mysql:3306" , "--strict" , "--timeout=300" , "--" , "npx", "nodemon", "src/index.js"]