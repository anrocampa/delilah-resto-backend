const bcrypt = require("bcryptjs");
const { getModel } = require("../model/index");
const { ROUNDS_BCRYPT } = require("../constants");

const adminData = {
  username: "admin",
  nombreApellido: "",
  email: "admin@dominio.com",
  direccion: "",
  telefono: "",
  clave: bcrypt.hashSync("lamegaclave", ROUNDS_BCRYPT),
  esAdmin: true,
};

async function initialize() {
  const usuario = getModel("Usuario");
  const current = await usuario.findOne({
    where: {
      username: "admin",
    },
  });
  if (!current) {
    await usuario.create(adminData);
  }
}

module.exports = {
  initialize,
};
