const express = require("express");
const { config } = require("dotenv");
const { prepareMiddlewares } = require("./controllers/middlewares/index");
const { createRouters } = require("./controllers/routers/index");
const { connect, redisConnect } = require("./model/index");
const { initialize } = require("./config/db");

const passport = require("passport");
const strategy = require("passport-facebook");
const FacebookStrategy = strategy.Strategy;

const initPassport = (FACEBOOK_CLIENT_ID, FACEBOOK_CLIENT_SECRET, FACEBOOK_CALLBACK_URL) => {
  passport.serializeUser(function (user, done) {
    done(null, user);
  });

  passport.deserializeUser(function (obj, done) {
    done(null, obj);
  });

  console.log(FACEBOOK_CLIENT_ID, FACEBOOK_CALLBACK_URL);
  passport.use(
    new FacebookStrategy(
      {
        clientID: FACEBOOK_CLIENT_ID,
        clientSecret: FACEBOOK_CLIENT_SECRET,
        callbackURL: FACEBOOK_CALLBACK_URL,
        profileFields: ["email", "name"],
      },
      function (accessToken, refreshToken, profile, done) {
        const { email, first_name, last_name } = profile._json;
        console.log("Got a profile from Facebook", profile._json);

        done(null, profile._json);
      }
    )
  );
};

const makeServer = () => {
  const server = express();
  prepareMiddlewares(server);
  // init Passport
  const { FACEBOOK_CLIENT_ID, FACEBOOK_CLIENT_SECRET, FACEBOOK_CALLBACK_URL } = global.process.env;
  initPassport(FACEBOOK_CLIENT_ID, FACEBOOK_CLIENT_SECRET, FACEBOOK_CALLBACK_URL);
  createRouters(server);
  return server;
};

async function main() {
  config();

  // init db
  const { DB_USERNAME, DB_PASSWORD, DB_NAME, DB_PORT, DB_HOST } = global.process.env;
  console.log("going to connect DB", DB_HOST, DB_PORT, DB_USERNAME, DB_NAME);
  await connect(DB_HOST, DB_PORT, DB_USERNAME, DB_PASSWORD, DB_NAME);

  // crear usuario admin
  initialize();

  // init Redis
  const { REDIS_HOST, REDIS_PORT } = global.process.env;
  redisConnect(REDIS_HOST, REDIS_PORT);

  const server = makeServer();

  const { SERVER_PORT } = global.process.env;
  server.listen(SERVER_PORT, () => {
    console.log(`Servidor backend Delilah en ejecución... ${SERVER_PORT}`);
  });
}

main();

module.exports = { makeServer };
