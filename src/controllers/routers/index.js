const { makeRouter: makeUsuariosRouter } = require("./usuarios");
const { makeRouter: makeProductosRouter } = require("./productos");
const { makeRouter: makePedidosRouter } = require("./pedidos");
const { makeRouter: makeMedioPagoRouter } = require("./medioDePagos");

function createRouters(server) {
  server.use("/", makeUsuariosRouter());
  server.use("/", makeProductosRouter());
  server.use("/", makeMedioPagoRouter());
  server.use("/", makePedidosRouter());
}
module.exports = { createRouters };
