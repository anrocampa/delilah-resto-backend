const { Router } = require("express");

//const { cache, cleanCache } = require('../middlewares/cache');

const { HTTP_STATUS } = require("../../constants");
const { esLogin, esAdmin, validoIdProducto } = require("../middlewares/middleware");

function makeRouter() {
  const router = Router();

  /**
   * @swagger
   * /productos:
   *  get:
   *    tags:
   *      - "Productos"
   *    summary: "Retorna todos los productos"
   *    description: Retorna un array de objetos con los productos del sistema
   *    parameters: []
   *    produces:
   *      - "application/json"
   *    responses:
   *      "200":
   *        description: "successful operation"
   *        schema:
   *          type: "string"
   *          example: [{ "id": int, "nombre": string, "precio": string, "fotoUrl": string }]
   *      "401":
   *        description: "Unauthorized"
   *        schema:
   *          type: "string"
   *          example: { "mensaje": "Usuario no logueado" }
   */

  router.get("/productos", esLogin, async (req, res) => {
    const db = require("../../model");

    const redis = db.getModel("Redis");
    const redisProductos = await redis.get("productos");

    if (redisProductos) {
      res.status(HTTP_STATUS.Ok).json(JSON.parse(redisProductos));
    } else {
      const producto = db.getModel("Producto");
      const data = await producto.findAll({
        attributes: ["id", "nombre", "precio", "fotoUrl"],
        raw: true,
      });

      redis.set("productos", JSON.stringify(data)); //store nuevos valores en cache

      res.status(HTTP_STATUS.Ok).json(data);
    }
  });

  /**
   * @swagger
   * /productos:
   *  post:
   *    tags:
   *      - "Productos"
   *    summary: "Crear un nuevo producto"
   *    description: Crea un producto en el sistema,no valida que el producto este repetido
   *    consumes:
   *      - "application/json"
   *    parameters:
   *    - name: body
   *      description: Objeto cuerpo de un producto
   *      in: body
   *      required: true
   *      type: "string"
   *      example: {"nombre": string, "precio": string, "fotoUrl": string}
   *    produces:
   *      - "application/json"
   *    responses:
   *      "201":
   *        description: "Producto creado"
   *        schema:
   *          type: "string"
   *          example: [{ "codigo" : codigoCreado, "mensaje" : "Producto creado" }]
   *      "401":
   *        description: "Unauthorized"
   *        schema:
   *          type: "string"
   *          example: { "mensaje": "Usuario no logueado" }
   *      "403":
   *        description: "Forbidden"
   *        schema:
   *          type: "string"
   *          example: { "mensaje": "No es Administrador" }
   */

  router.post("/productos", esLogin, esAdmin, async (req, res) => {
    let { nombre, precio, fotoUrl } = req.body;

    const db = require("../../model");
    const producto = db.getModel("Producto");
    const nuevoProducto = await producto.create({
      nombre: nombre,
      precio: precio,
      fotoUrl: fotoUrl,
    });

    // limpia cache
    const redis = db.getModel("Redis");
    await redis.del("productos");

    res.status(HTTP_STATUS.Created).json({ codigo: nuevoProducto.id, mensaje: "Producto creado" });
  });

  /**
   * @swagger
   * /productos/{codigo}:
   *  put:
   *    tags:
   *      - "Productos"
   *    summary: "Modifica un producto"
   *    description: Modifica un producto en el sistema
   *    consumes:
   *      - "application/json"
   *    parameters:
   *    - name: "codigo"
   *      in: "path"
   *      description: "codigo del producto a actualizar"
   *      required: true
   *      type: "string"
   *    - name: body
   *      description: Objeto cuerpo de un producto
   *      in: body
   *      required: true
   *      type: "string"
   *      example: { "nombre": string, "precio": string, "fotoUrl": string }
   *    produces:
   *      - "application/json"
   *    responses:
   *      "201":
   *        description: "Producto actualizado"
   *        schema:
   *          type: "string"
   *          example: { "mensaje": "Producto actualizado" }
   *      "401":
   *        description: "Unauthorized"
   *        schema:
   *          type: "string"
   *          example: { "mensaje": "Usuario no logueado" }
   *      "403":
   *        description: "Forbidden"
   *        schema:
   *          type: "string"
   *          example: { "mensaje": "No es Administrador" }
   *      "404":
   *        description: "NotFound"
   *        schema:
   *          type: "string"
   *          example: { "mensaje": "Producto no encontrado" }
   *
   */
  router.put("/productos/:codigo", esLogin, esAdmin, validoIdProducto, async (req, res) => {
    let { codigo } = req.params;
    let { nombre, precio, fotoUrl } = req.body;

    const db = require("../../model");
    const producto = db.getModel("Producto");
    await producto.update(
      { nombre: nombre, precio: precio, fotoUrl: fotoUrl },
      {
        where: {
          id: codigo,
        },
      }
    );

    // limpia cache
    const redis = db.getModel("Redis");
    await redis.del("productos");

    res.status(HTTP_STATUS.Ok).json({ mensaje: "Producto actualizado" });
  });

  /**
   * @swagger
   * /productos/{codigo}:
   *  delete:
   *    tags:
   *      - "Productos"
   *    summary: "Elimina un producto"
   *    description: Elimina un producto en el sistema
   *    consumes:
   *      - "application/json"
   *    parameters:
   *    - name: "codigo"
   *      in: "path"
   *      description: "codigo del producto a eliminar"
   *      required: true
   *      type: "string"
   *    - name: body
   *      description: Objeto cuerpo de un producto
   *      in: body
   *      required: true
   *      type: "string"
   *      example: { "nombre": string, "precio": string, "fotoUrl": string }
   *    produces:
   *      - "application/json"
   *    responses:
   *      "200":
   *        description: "Producto eliminado"
   *        schema:
   *          type: "string"
   *          example: { "mensaje": "Producto eliminado" }
   *      "401":
   *        description: "Unauthorized"
   *        schema:
   *          type: "string"
   *          example: { "mensaje": "Usuario no logueado" }
   *      "403":
   *        description: "Forbidden"
   *        schema:
   *          type: "string"
   *          example: { "mensaje": "No es Administrador" }
   *      "404":
   *        description: "NotFound"
   *        schema:
   *          type: "string"
   *          example: { "mensaje": "Producto no encontrado" }
   */
  router.delete("/productos/:codigo", esLogin, esAdmin, validoIdProducto, async (req, res) => {
    let { codigo } = req.params;

    const db = require("../../model");
    const producto = db.getModel("Producto");
    await producto.destroy({
      where: {
        id: codigo,
      },
    });

    // limpia cache
    const redis = db.getModel("Redis");
    await redis.del("productos");

    res.status(HTTP_STATUS.Ok).json({ mensaje: "Producto eliminado" });
  });

  return router;
}

module.exports = {
  makeRouter,
};
