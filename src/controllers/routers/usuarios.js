const { Router } = require("express");
const passport = require("passport");
const jwt = require("jsonwebtoken");
const bcrypt = require("bcryptjs");
const { Op } = require("sequelize");

const db = require("../../model");

const { HTTP_STATUS, ROUNDS_BCRYPT } = require("../../constants");
const { esLogin, esAdmin } = require("../middlewares/middleware");

function makeRouter() {
  const router = Router();

  const creoJWT = (username) => {
    // el JWT contiene el username del usuario
    const datosJWT = {
      username: username,
    };

    const JWT_PASS = global.process.env.JWT_PASS;
    return jwt.sign(datosJWT, JWT_PASS);
  };

  const passportErrorHandler = (err, req, res, next) => {
    // Here you can handle passport errors
    console.error(`Passport error: ${err.message}`);
    res.redirect("/fail");
  };

  router.get(
    "/auth/facebook",
    passport.authenticate("facebook", { session: false, scope: ["email"] }, passportErrorHandler)
  );

  /**
   * @swagger
   * /auth/facebook/callback:
   *  get:
   *    tags:
   *      - "Usuarios"
   *    summary: "Login con Facebook"
   *    description: Retorna un objecto con token de login para ser usado por la aplicación cliente
   *    produces:
   *    - "application/json"
   *    responses:
   *      "200":
   *        description: "successful operation"
   *        schema:
   *          type: "string"
   *          example: { instructions: "Please, do the client work and use this token in Postman to fetch the endpoints", token: "xxx.yyy.zzz", mensaje: "Login correcto" }
   *      "403":
   *        description: "Unauthorized"
   *        schema:
   *          type: "string"
   *          example: { "mensaje": "Usuario existente registrado con usuario y clave, inicie session de esa forma" }
   *      "401":
   *        description: "Forbidden"
   *        schema:
   *          type: "string"
   *          example: { "mensaje": "login incorrecto" }
   */

  router.get(
    "/auth/facebook/callback",
    passport.authenticate("facebook", {
      successRedirect: "/success",
      failureRedirect: "/fail",
    })
  );

  router.get("/fail", (req, res) => {
    res.status(HTTP_STATUS.Forbidden).json({ mensaje: "login incorrecto" });
  });

  router.get("/success", async (req, res) => {
    if (req.isAuthenticated()) {
      const { email, last_name, first_name, id } = req.user;

      const usuario = db.getModel("Usuario");

      const data = await usuario.findAll({
        raw: true,
        where: {
          email: email,
        },
      });

      if (data.length > 0 && !!data[0].clave) {
        res.status(HTTP_STATUS.Forbidden).json({
          mensaje: "Usuario existente registrado con usuario y clave, inicie session de esa forma",
        });
      } else if (data.length > 0 && data[0].id_facebook) {
        // Usuario registrado con Facebook existe,

        res.status(HTTP_STATUS.Ok).json({
          instructions: "Please, do the client work and use this token in Postman to fetch the endpoints",
          token: creoJWT(data[0].email),
          mensaje: "Login correcto",
        });
      } else if (data.length === 0) {
        // Usuario registrado con Facebook NO existe, lo creo
        await usuario.create({
          username: email,
          nombreApellido: `${first_name} ${last_name}`,
          email: email,
          direccion: "",
          telefono: "",
          clave: "",
          id_facebook: id,
          esAdmin: false,
        });

        res.status(HTTP_STATUS.Ok).json({
          instructions: "Please, do the client work and use this token in Postman to fetch the endpoints",
          token: creoJWT(email),
          mensaje: "Login correcto",
        });
      }
    } else {
      res.status(HTTP_STATUS.Forbidden).json({ mensaje: "login incorrecto" });
    }
  });

  //AWS HealthCheck
  router.get("/", (req, res) => {
    res.status(HTTP_STATUS.Ok).send("<h1> andreacampanella.tk </h1>");
  });

  /**
   * @swagger
   * /usuarios:
   *  get:
   *    tags:
   *      - "Usuarios"
   *    summary: "Retorna todos los usuarios"
   *    description: Retorna un array de objetos con los usuarios del sistema
   *    parameters: []
   *    produces:
   *      - "application/json"
   *    responses:
   *      "200":
   *        description: "successful operation"
   *        schema:
   *          type: "string"
   *          example: [{ "nombreApellido": string, "email": string, "direccion": string, "telefono": string, "username": string }]
   *      "401":
   *        description: "Unauthorized"
   *        schema:
   *          type: "string"
   *          example: { "mensaje": "Usuario no logueado" }
   *      "403":
   *        description: "Forbidden"
   *        schema:
   *          type: "string"
   *          example: { "mensaje": "No es Administrador" }
   */
  router.get("/usuarios", esLogin, esAdmin, async (req, res) => {
    const usuario = db.getModel("Usuario");
    const data = await usuario.findAll({
      raw: true,
      attributes: ["username", "nombreApellido", "email", "direccion", "telefono", "id_facebook", "activo"],
    });

    res.status(HTTP_STATUS.Ok).json(data);
  });

  /**
   * @swagger
   * /usuarios:
   *  post:
   *    tags:
   *      - "Usuarios"
   *    summary: "Crear un usuario"
   *    description: Crea un usuario en el sistema, valida que el username o email no esté duplicado
   *    consumes:
   *      - "application/json"
   *    parameters:
   *    - name: body
   *      description: Objeto cuerpo de un usuario
   *      in: body
   *      required: true
   *      type: "string"
   *      example: { "nombreApellido": string, "email": string, "direccion": string, "telefono": string, "username": string, "clave": string}
   *    produces:
   *      - "application/json"
   *    responses:
   *      "201":
   *        description: "Created"
   *        schema:
   *          type: "string"
   *          example: { "mensaje":  "Usuario creado" }
   *      "403":
   *        description: "Forbidden"
   *        schema:
   *          type: "string"
   *          example: { "mensaje": "Usuario duplicado" }
   */
  router.post("/usuarios", async (req, res) => {
    let { nombreApellido, email, direccion, telefono, username, clave } = req.body;

    if (username && email && nombreApellido && clave && direccion) {
      const usuario = db.getModel("Usuario");
      const data = await usuario.findAll({
        raw: true,
        where: {
          [Op.or]: [{ username: username }, { email: email }],
        },
      });

      if (data.length > 0) {
        res.status(HTTP_STATUS.Forbidden).json({ mensaje: "Usuario duplicado" });
      } else {
        const password_hash = bcrypt.hashSync(clave, ROUNDS_BCRYPT);
        await usuario.create({
          username: username,
          nombreApellido: nombreApellido,
          email: email,
          direccion: direccion,
          telefono: telefono,
          clave: password_hash,
          id_facebook: "",
          esAdmin: false,
        });

        res.status(HTTP_STATUS.Created).json({ mensaje: "Usuario creado" });
      }
    } else {
      res.status(HTTP_STATUS.Forbidden).json({ mensaje: "Datos inválidos" });
    }
  });

  /**
   * @swagger
   * /login:
   *  post:
   *    tags:
   *      - "Usuarios"
   *    summary: "Hacer login de un usuario"
   *    description: Loguea un usuario al sistema, valida que el usuario exista y la clave sea correcta
   *    consumes:
   *      - "application/json"
   *    parameters:
   *    - name: body
   *      description: Objeto cuerpo con datos de login
   *      in: body
   *      required: true
   *      type: "string"
   *      example: { "username": string, "clave": string }
   *    produces:
   *      - "application/json"
   *    responses:
   *      "200":
   *        description: "Login correcto"
   *        schema:
   *          type: "string"
   *          example: { "mensaje": "Login correcto" }
   *      "401":
   *        description: "Usuario o clave incorrecta"
   *        schema:
   *          type: "string"
   *          example: { "mensaje": "Usuario o clave incorrecta" }
   */
  router.post("/login", async (req, res) => {
    let { username, clave } = req.body;

    const usuario = db.getModel("Usuario");
    const data = await usuario.findAll({
      raw: true,
      where: {
        [Op.and]: [{ [Op.or]: [{ username: username }, { email: username }] }, { activo: true }],
      },
    });

    const verified = data.length > 0 ? bcrypt.compareSync(clave, data[0].clave) : false;

    if (verified) {
      // el JWT contiene el username del usuario
      const datosJWT = {
        username: data[0].username,
      };

      const JWT_PASS = global.process.env.JWT_PASS;
      const tokenJwt = jwt.sign(datosJWT, JWT_PASS);

      res.status(HTTP_STATUS.Ok).json({ codigo: tokenJwt, mensaje: "Login correcto" });
    } else {
      res.status(HTTP_STATUS.Unauthorized).json({ mensaje: "Usuario o Clave incorrecta" });
    }
  });

  /**
   * @swagger
   * /usuarios/{username}/enable:
   *  post:
   *    tags:
   *      - "Usuarios"
   *    summary: "Activa un usuario"
   *    description: Activa un usuario
   *    consumes:
   *      - "application/json"
   *    parameters:
   *    - name: "username"
   *      in: "path"
   *      description: "username del usuario a activar"
   *      required: true
   *      type: "string"
   *    produces:
   *      - "application/json"
   *    responses:
   *      "200":
   *        description: "Usuario activado correctamente"
   *        schema:
   *          type: "string"
   *          example: { "mensaje": "Usuario activado" }
   *      "404":
   *        description: "Usuario no encontrado"
   *        schema:
   *          type: "string"
   *          example: { "mensaje": "Usuario no encontrado" }
   */
  router.post("/usuarios/:username/enable", esLogin, esAdmin, async (req, res) => {
    let { username } = req.params;

    const db = require("../../model");
    const usuarios = db.getModel("Usuario");
    const data = await usuarios.update(
      { activo: true },
      {
        where: {
          username: username,
        },
      }
    );

    if (data[0]) {
      res.status(HTTP_STATUS.Ok).json({ mensaje: "Usuario activado" });
    } else {
      res.status(HTTP_STATUS.NotFound).json({ mensaje: "Usuario no encontrado" });
    }
  });

  /**
   * @swagger
   * /usuarios/{username}/disable:
   *  post:
   *    tags:
   *      - "Usuarios"
   *    summary: "Inactiva un usuario"
   *    description: Inactiva un usuario
   *    consumes:
   *      - "application/json"
   *    parameters:
   *    - name: "username"
   *      in: "path"
   *      description: "username del usuario a desactivar"
   *      required: true
   *      type: "string"
   *    produces:
   *      - "application/json"
   *    responses:
   *      "200":
   *        description: "Usuario desactivado correctamente"
   *        schema:
   *          type: "string"
   *          example: { "mensaje": "Usuario desactivado" }
   *      "404":
   *        description: "Usuario no encontrado"
   *        schema:
   *          type: "string"
   *          example: { "mensaje": "Usuario no encontrado" }
   */
  router.post("/usuarios/:username/disable", esLogin, esAdmin, async (req, res) => {
    let { username } = req.params;

    const db = require("../../model");
    const usuarios = db.getModel("Usuario");
    const data = await usuarios.update(
      { activo: false },
      {
        where: {
          username: username,
        },
      }
    );

    if (data[0]) {
      res.status(HTTP_STATUS.Ok).json({ mensaje: "Usuario desactivado" });
    } else {
      res.status(HTTP_STATUS.NotFound).json({ mensaje: "Usuario no encontrado" });
    }
  });
  return router;
}

module.exports = {
  makeRouter,
};
