const { Router } = require("express");

const { esLogin, esAdmin, validoIdMedioPago } = require("../middlewares/middleware");
const { HTTP_STATUS } = require("../../constants");

function makeRouter() {
  const router = Router();
  /**
   * @swagger
   * /medioDePago:
   *  get:
   *    tags:
   *      - "Medios de Pago"
   *    summary: "Retorna todos los medios de pagos"
   *    description: Retorna un array de objetos con los medios de pagos
   *    parameters: []
   *    produces:
   *      - "application/json"
   *    responses:
   *      "200":
   *        description: "successful operation"
   *        schema:
   *          type: "string"
   *          example: [{ "id": int, "nombre": string, "descripcion": string, "icono": string }]
   *      "401":
   *        description: "Unauthorized"
   *        schema:
   *          type: "string"
   *          example: { "mensaje": "Usuario no logueado" }
   *      "403":
   *        description: "Forbidden"
   *        schema:
   *          type: "string"
   *          example: { "mensaje": "No es Administrador" }
   */
  router.get("/medioDePago", esLogin, esAdmin, async (req, res) => {
    const db = require("../../model");
    const medioPago = db.getModel("MedioDePago");
    const data = await medioPago.findAll({ raw: true });

    res.status(HTTP_STATUS.Ok).json(data);
  });

  /**
   * @swagger
   * /medioDePago:
   *  post:
   *    tags:
   *      - "Medios de Pago"
   *    summary: "Crear un nuevo medioDePago"
   *    description: Crea un medioDePago en el sistema,no valida que el medioDePago  este repetido
   *    consumes:
   *      - "application/json"
   *    parameters:
   *    - name: body
   *      description: Objeto cuerpo de un medioDePago
   *      in: body
   *      required: true
   *      type: "string"
   *      example: {"nombre": string, "descripcion": string, "icono": string}
   *    produces:
   *      - "application/json"
   *    responses:
   *      "201":
   *         description: "MedioDePago creado"
   *         schema:
   *           type: "string"
   *           example: { "mensaje": "MedioDePago creado" }
   *      "401":
   *         description: "Unauthorized"
   *         schema:
   *           type: "string"
   *           example: { "mensaje": "Usuario no logueado" }
   *      "403":
   *         description: "Forbidden"
   *         schema:
   *           type: "string"
   *           example: { "mensaje": "No es Administrador" }
   */
  router.post("/medioDePago", esLogin, esAdmin, async (req, res) => {
    let { nombre, descripcion, icono } = req.body;

    // traer el modelo y
    // cambiar por build y save
    const db = require("../../model");
    const mediopago = db.getModel("MedioDePago");
    const nuevoMP = await mediopago.create({
      nombre: nombre,
      descripcion: descripcion,
      icono: icono,
    });

    res.status(HTTP_STATUS.Created).json({ codigo: nuevoMP.id, mensaje: "Medio de pago creado" });
  });

  /**
   * @swagger
   * /medioDePago/{codigo}:
   *  put:
   *    tags:
   *      - "Medios de Pago"
   *    summary: "Modifica un medio de pago"
   *    description: Modifica un medio de pago en el sistema, valida que el username o email no esté duplicado
   *    consumes:
   *      - "application/json"
   *    parameters:
   *    - name: "codigo"
   *      in: "path"
   *      description: "codigo del medio de pago a actualizar"
   *      required: true
   *      type: "string"
   *    - name: body
   *      description: Objeto cuerpo de un medio de pago
   *      in: body
   *      required: true
   *      type: "string"
   *      example: { "nombre": string, "descripcion": string, "icono": string }
   *    produces:
   *      - "application/json"
   *    responses:
   *      "200":
   *        description: "MedioDePago actualizado"
   *        schema:
   *          type: "string"
   *          example: { "mensaje": "MedioDePago actualizado" }
   *      "401":
   *        description: "Unauthorized"
   *        schema:
   *          type: "string"
   *          example: { "mensaje": "Usuario no logueado" }
   *      "403":
   *        description: "Forbidden"
   *        schema:
   *          type: "string"
   *          example: { "mensaje": "No es Administrador" }
   *      "404":
   *        description: "NotFound"
   *        schema:
   *          type: "string"
   *          example: { "mensaje": "MedioDePago no encontrado" }
   */
  router.put("/medioDePago/:codigo", esLogin, esAdmin, validoIdMedioPago, async (req, res) => {
    let { codigo } = req.params;
    let { nombre, descripcion, icono } = req.body;

    const db = require("../../model");
    const mediopago = db.getModel("MedioDePago");
    await mediopago.update(
      { nombre: nombre, descripcion: descripcion, icono: icono },
      {
        where: {
          id: codigo,
        },
      }
    );
    res.status(HTTP_STATUS.Ok).json({ mensaje: "Medio de pago actualizado" });
  });

  /**
   * @swagger
   * /medioDePago/{codigo}:
   *  delete:
   *    tags:
   *      - "Medios de Pago"
   *    summary: "Elimina un medio de pago"
   *    description: Elimina un medio de pago en el sistema
   *    consumes:
   *      - "application/json"
   *    parameters:
   *    - name: "codigo"
   *      in: "path"
   *      description: "codigo del medio de pago a eliminar"
   *      required: true
   *      type: "string"
   *    - name: body
   *      description: Objeto cuerpo de un medio de pago
   *      in: body
   *      required: true
   *      type: "string"
   *      example: { "nombre": string, "descripcion": string, "icono": string }
   *    produces:
   *      - "application/json"
   *    responses:
   *      "200":
   *        description: "MedioDePago eliminado"
   *        schema:
   *          type: "string"
   *          example: { "mensaje": "MedioDePago eliminado" }
   *      "401":
   *        description: "Unauthorized"
   *        schema:
   *          type: "string"
   *          example: { "mensaje": "Usuario no logueado" }
   *      "403":
   *        description: "Forbidden"
   *        schema:
   *          type: "string"
   *          example: { "mensaje": "No es Administrador" }
   *      "404":
   *        description: "NotFound"
   *        schema:
   *          type: "string"
   *          example: { "mensaje": "MedioDePago no encontrado" }
   */
  router.delete("/medioDePago/:codigo", esLogin, esAdmin, validoIdMedioPago, async (req, res) => {
    let { codigo } = req.params;

    const db = require("../../model");
    const mediopago = db.getModel("MedioDePago");
    await mediopago.destroy({
      where: {
        id: codigo,
      },
    });

    res.status(HTTP_STATUS.Ok).json({ mensaje: "Medio de pago eliminado" });
  });

  return router;
}

module.exports = {
  makeRouter,
};
