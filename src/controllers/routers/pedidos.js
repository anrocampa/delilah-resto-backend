const { Router } = require("express");
const mercadopago = require("mercadopago");

const { PEDIDOS_STATUS, HTTP_STATUS } = require("../../constants");
const db = require("../../model");

mercadopago.configure({
  access_token: global.process.env.MP_TOKEN,
});

const { esLogin, esAdmin, validoIdPedido } = require("../middlewares/middleware");

function makeRouter() {
  const router = Router();

  /**
   * @swagger
   * /pagar/{codigo}:
   *  post:
   *    tags:
   *      - "Pagar"
   *    summary: "Pagar por MercadoPago"
   *    description: Retorna un objecto con url de pago
   *    consumes:
   *      - "application/json"
   *    parameters:
   *    - name: "codigo"
   *      in: "path"
   *      description: "codigo del pedido a pagar"
   *      required: true
   *      type: "integer"
   *    produces:
   *    - "application/json"
   *    responses:
   *      "200":
   *        description: "successful operation"
   *        schema:
   *          type: "string"
   *          example: { instructions:  "Please, do the client work and open this url in your browser to complete the payment", preference_id: "id", url: "url"}
   *      "403":
   *        description: "Forbidden"
   *        schema:
   *          type: "string"
   *          example: { "mensaje": "Petición denegada, pedido debe de estar confirmado" }
   *      "401":
   *        description: "Unauthorized"
   *        schema:
   *          type: "string"
   *          example: { "mensaje": "Petición denegada, Autorización no valida" }
   */
  router.post("/pagar/:codigo", esLogin, validoIdPedido, (req, res) => {
    let { codigo } = req.params; // codigo del pedido
    let { username, pedidoOriginal, logedUser, pedidoLineas } = req.headers; // quien está pidiendo, y el objeto del pedido a modificar

    console.log("New request POST to /pagar", codigo, logedUser);

    if (pedidoOriginal.estado.toUpperCase() !== PEDIDOS_STATUS.Confirmado) {
      res.status(HTTP_STATUS.Forbidden).json({
        mensaje: "Petición denegada, pedido debe de estar confirmado",
      });
    } else {
      if (pedidoOriginal.username === username) {
        const user = {
          first_name: logedUser.nombreApellido,
          last_name: "",
          email: logedUser.email,
        };

        const items = pedidoLineas.map((linea) => {
          if (linea && linea.Productos && linea.Productos.PedidoProducto) {
            return {
              title: linea.Productos.nombre,
              unit_price: parseInt(linea.Productos.precio),
              quantity: linea.Productos.PedidoProducto.cantidad,
            };
          }
        });

        // Crea un objeto de preferencia
        let preference = {
          auto_return: "approved",
          back_urls: {
            success: `${global.process.env.MP_REDIRECT_SERVER_URL}/mp_check?`,
            failure: `${global.process.env.MP_REDIRECT_SERVER_URL}/mp_check?`,
            pending: `${global.process.env.MP_REDIRECT_SERVER_URL}/mp_check?`,
          },
          payer: {
            name: user.first_name,
            surname: user.last_name,
            email: user.email,
          },
          items,
        };

        // petición a mercado pago para preparar la compra
        mercadopago.preferences.create(preference).then((response) => {
          let id = response.body.id;

          res.status(HTTP_STATUS.Ok).json({
            instructions:
              "Please, do the client work and open this url in your browser to complete the payment",
            preference_id: id,
            url: response.body.sandbox_init_point,
          });
        });
      } else {
        console.log("Usuarios difieren: Pedido: ", pedidoOriginal.username, ", JWT:", username);
        res.status(HTTP_STATUS.Unauthorized).json({ mensaje: "Petición denegada, Autorización no valida" });
      }
    }
  });

  router.get("/mp_check", (req, res) => {
    res.status(HTTP_STATUS.Ok).json(req.query);
  });

  /**
   * @swagger
   * /pedidos:
   *  get:
   *    tags:
   *      - "Pedidos"
   *    summary: "Retorna todos los pedidos"
   *    description: Retorna un array de objetos con los pedidos del sistema
   *    parameters: []
   *    produces:
   *    - "application/json"
   *    responses:
   *      "200":
   *        description: "successful operation"
   *        schema:
   *          type: "string"
   *          example: {"id": int, "formaPago": int, "estado": string, "productos": string,"direccion": string,"fecha": datetime,"username": string }
   *      "401":
   *        description: "Unauthorized"
   *        schema:
   *          type: "string"
   *          example: { "mensaje": "Usuario no logueado" }
   */
  router.get("/pedidos", esLogin, async (req, res) => {
    const username = req.headers.username;

    const usuario = db.getModel("Usuario");
    let dataUsuario = await usuario.findAll({ raw: true, where: { username: username } });

    const pedido = db.getModel("Pedido");
    const producto = db.getModel("Producto");

    let dataPedido = [];

    if (dataUsuario[0] && dataUsuario[0].esAdmin) {
      dataPedido = await pedido.findAll({
        attributes: ["id", "formaPago", "direccion", "fecha", "estado"],
        include: [
          {
            model: producto,
            required: false,
            attributes: ["id", "nombre", "precio"],
            through: {
              attributes: ["cantidad"],
            },
          },
        ],
      });
    } else {
      dataPedido = await pedido.findAll({
        attributes: ["id", "formaPago", "direccion", "fecha", "estado"],
        include: [
          {
            model: producto,
            required: false,
            attributes: ["id", "nombre", "precio"],
            through: {
              attributes: ["cantidad"],
            },
          },
        ],
        where: { username: username },
      });
    }
    //TODO agregar devolver el username
    res.status(HTTP_STATUS.Ok).json(dataPedido);
  });

  /**
   * @swagger
   * /pedidos:
   *  post:
   *    tags:
   *      - "Pedidos"
   *    summary: "Crear un nuevo pedido"
   *    description: Crea un pedido en el sistema
   *    consumes:
   *    - "application/json"
   *    parameters:
   *    - name: body
   *      description: Objeto cuerpo de un pedido
   *      in: body
   *      required: true
   *      type: "string"
   *      example: { "formaPago": int, "productos": string, "direccion": string }
   *    produces:
   *      - "application/json"
   *    responses:
   *      "201":
   *        description: "Pedido creado"
   *        schema:
   *          type: "string"
   *          example: { "mensaje": "Pedido creado" }
   *      "401":
   *        description: "Unauthorized"
   *        schema:
   *          type: "string"
   *          example: { "mensaje": "Usuario no logueado" }
   */
  router.post("/pedidos", esLogin, async (req, res) => {
    let { username } = req.headers; // quien está pidiendo
    let { formaPago, direccion, productos } = req.body;

    if (!direccion) {
      const usuario = db.getModel("Usuario");
      const miUsuario = await usuario.findAll({ raw: true, where: { username: username } });
      direccion = miUsuario[0].direccion; // obtener la direccion del usuario
    }

    //agrego el pedido a la lista y obtengo el id del pedido nuevo
    const pedido = db.getModel("Pedido");
    const nuevoPedido = await pedido.create({
      formaPago: formaPago,
      direccion: direccion,
      fecha: new Date(),
      username: username,
    });

    const pedidoProducto = db.getModel("PedidoProducto");

    productos.forEach(async (linea) => {
      try {
        await pedidoProducto.create({
          PedidoId: nuevoPedido.id,
          ProductoId: linea.codigo,
          cantidad: linea.cantidad,
        });
      } catch {
        console.log("POST pedidos - Producto no encontrado ");
      }
    });

    res.status(HTTP_STATUS.Created).json({ codigo: nuevoPedido.id, mensaje: "Pedido creado" });
  });
  /**
   * @swagger
   * /pedidos/{codigo}/{estado}:
   *  post:
   *    tags:
   *      - "Pedidos"
   *    summary: "Cambiar estado de un pedido"
   *    description: Cambia el estado de un pedido en el sistema
   *    consumes:
   *      - "application/json"
   *    parameters:
   *    - name: "codigo"
   *      in: "path"
   *      description: "codigo del pedido a modificar"
   *      required: true
   *      type: "string"
   *    - name: "estado"
   *      in: path
   *      description: "estado del pedido a modificar"
   *      required: true
   *      type: "string"
   *      enum:
   *        - Pendiente
   *        - Confirmado
   *        - Preparacion
   *        - Enviado
   *        - Entregado
   *    produces:
   *      - "application/json"
   *    responses:
   *       "200":
   *        description: "Estado creado"
   *        schema:
   *          type: "string"
   *          example: { "mensaje": "Estado actualizado" }
   *       "401":
   *        description: "Unauthorized"
   *        schema:
   *          type: "string"
   *          example: { "mensaje": "Usuario no logueado" }
   *       "403":
   *        description: "Forbidden"
   *        schema:
   *          type: "string"
   *          example: { "mensaje": "No es Administrador" }
   *       "404":
   *        description: "NotFound"
   *        schema:
   *          type: "string"
   *          example: { "mensaje": "Estado no encontrado" }
   */
  router.post("/pedidos/:codigo/:estado", esLogin, esAdmin, validoIdPedido, async (req, res) => {
    let { codigo } = req.params; // codigo del pedido
    let { estado } = req.params; // proximo estado del pedido
    //TODO verificar que el estado sea modificable (ver entregable de sprint1)
    if (estado in PEDIDOS_STATUS) {
      const pedido = db.getModel("Pedido");
      await pedido.update(
        { estado: estado.toUpperCase() },
        {
          where: {
            id: codigo,
          },
        }
      );

      res.status(HTTP_STATUS.Ok).json({ mensaje: "Pedido actualizado" });
    } else {
      res.status(HTTP_STATUS.BadRequest).json({ mensaje: "Estado incorrecto" });
    }
  });

  /**
   * @swagger
   * /pedidos/{codigo}:
   *  put:
   *    tags:
   *      - "Pedidos"
   *    summary: "Modificar un pedido"
   *    description: Modifica un pedido en el sistema, valida que el codigo exista en el sistema
   *    consumes:
   *      - "application/json"
   *    parameters:
   *    - name: "codigo"
   *      in: "path"
   *      description: "codigo del pedido a actualizar"
   *      required: true
   *      type: "string"
   *    - name: body
   *      description: Objeto cuerpo de un pedido
   *      in: body
   *      required: true
   *      type: "string"
   *      example:  { "formaPago": int, "productos": int, "direccion": string }
   *    produces:
   *      - "application/json"
   *    responses:
   *      "200":
   *        description: "Pedido actualizado"
   *        schema:
   *          type: "string"
   *          example: { "mensaje": "Pedido actualizado" }
   *      "401":
   *        description: "Unauthorized"
   *        schema:
   *          type: "string"
   *          example: { "mensaje": "Pedido no logueado" }
   *      "404":
   *        description: "NotFound"
   *        schema:
   *          type: "string"
   *          example: { "mensaje": "Pedido no encontrado" }
   *
   */
  router.put("/pedidos/:codigo", esLogin, validoIdPedido, async (req, res) => {
    let { username, pedidoOriginal } = req.headers; // quien está pidiendo, y el objeto del pedido a modificar
    let { codigo } = req.params; // codigo del pedido

    let { formaPago, productos, direccion } = req.body;

    if (pedidoOriginal.estado.toUpperCase() !== PEDIDOS_STATUS.Pendiente) {
      res
        .status(HTTP_STATUS.Forbidden)
        .json({ mensaje: "Petición denegada, pedido debe de estar pendiente" });
    } else {
      if (pedidoOriginal.username === username) {
        const pedido = db.getModel("Pedido");
        await pedido.update({ formaPago: formaPago, direccion: direccion }, { where: { id: codigo } });

        const pedidoProducto = db.getModel("PedidoProducto");
        await pedidoProducto.destroy({
          where: {
            PedidoId: codigo,
          },
        });

        productos.forEach(async (linea) => {
          await pedidoProducto.create({
            PedidoId: codigo,
            ProductoId: linea.codigo,
            cantidad: linea.cantidad,
          });
        });

        res.status(HTTP_STATUS.Ok).json({ mensaje: "Pedido actualizado" });
      } else {
        res.status(HTTP_STATUS.Forbidden).json({ mensaje: "Autorización no valida" });
      }
    }
  });

  return router;
}

module.exports = {
  makeRouter,
};
