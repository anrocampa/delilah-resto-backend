const jwt = require("jsonwebtoken");
const { HTTP_STATUS } = require("../../constants");

const { Op } = require("sequelize");

// middleware local para verificar si el usuario está logueado
const esLogin = (req, res, next) => {
  let tokenDeLogin = req.headers.token;

  const JWT_PASS = global.process.env.JWT_PASS;

  jwt.verify(tokenDeLogin, JWT_PASS, async (error, decoded) => {
    if (error) {
      res.status(HTTP_STATUS.Unauthorized).send({ mensaje: "Usuario no logueado" });
    } else {
      // una vez decoded el jwt, guardo el username para poder usarlo en siguientes middlewares
      req.headers.username = decoded.username;

      const db = require("../../model");
      const usuario = db.getModel("Usuario");
      const data = await usuario.findAll({
        raw: true,
        where: { username: decoded.username },
      });
      if (data.length > 0) {
        req.headers.logedUser = data[0];
        next();
      } else {
        res.status(HTTP_STATUS.Forbidden).send({ mensaje: "Usuario no logueado" }); // usuario no existe
      }
    }
  });
};

// middleware local para verificar si el usuario es administrador
const esAdmin = async (req, res, next) => {
  let { username } = req.headers; // req.headers.username lo validamos en el middleware login

  //buscar si el username tiene esAdmin en true
  const db = require("../../model");
  const usuario = db.getModel("Usuario");
  const data = await usuario.findAll({
    raw: true,
    where: {
      [Op.and]: [{ username: username }, { esAdmin: true }],
    },
  });

  if (data.length > 0) {
    next();
  } else {
    res.status(HTTP_STATUS.Forbidden).send({ mensaje: "No es Administrador" });
  }
};

// middleware local para verificar si el producto a modificar o eliminar existe
const validoIdProducto = async (req, res, next) => {
  let codigo = parseInt(req.params.codigo);

  const db = require("../../model");
  const producto = db.getModel("Producto");
  const data = await producto.findAll({
    raw: true,
    where: {
      id: codigo,
    },
  });

  if (data.length > 0) {
    req.params.codigo = codigo; //req.params.codigo lo actualizo con (parseInt) para poder usarlo en los endpoint
    next();
  } else {
    res.status(HTTP_STATUS.NotFound).json({ mensaje: "Producto no encontrado" });
  }
};

// middleware local para verificar si el pedido a modificar o eliminar existe
const validoIdPedido = async (req, res, next) => {
  let codigo = parseInt(req.params.codigo);
  const db = require("../../model");

  const pedido = db.getModel("Pedido");
  const producto = db.getModel("Producto");

  const dataPedido = await pedido.findAll({
    raw: true,
    where: {
      id: codigo,
    },
  });

  // obtengo las lineas del pedido, asi ya lo tengo disponible
  const dataLineas = await pedido.findAll({
    attributes: ["id", "formaPago", "direccion", "fecha", "estado"],
    include: [
      {
        model: producto,
        required: false,
        attributes: ["id", "nombre", "precio"],
        through: {
          attributes: ["cantidad"],
        },
      },
    ],
    where: { id: codigo },
    raw: true,
    nest: true,
  });
  if (dataPedido.length > 0) {
    req.params.codigo = codigo; //req.params.codigo lo actualizo con (pareseInt) para poder usarlo en los endpoint
    req.headers.pedidoOriginal = dataPedido[0]; // guardo el pedido
    req.headers.pedidoLineas = dataLineas; // guardo las lineas de productos

    next();
  } else {
    res.status(HTTP_STATUS.NotFound).json({ mensaje: "Pedido no encontrado" });
  }
};

// middleware local para verificar si el medioDePago a modificar o eliminar existe
const validoIdMedioPago = async (req, res, next) => {
  let codigo = parseInt(req.params.codigo);
  const db = require("../../model");
  const medioPago = db.getModel("MedioDePago");
  const data = await medioPago.findAll({
    raw: true,
    where: {
      id: codigo,
    },
  });

  if (data.length > 0) {
    req.params.codigo = codigo; //req.params.codigo lo actualizo con (parseInt) para poder usarlo en los endpoint
    next();
  } else {
    res.status(HTTP_STATUS.NotFound).json({ mensaje: "Medio de pago no encontrado" });
  }
};

module.exports = {
  esLogin,
  esAdmin,
  validoIdProducto,
  validoIdPedido,
  validoIdMedioPago,
};
