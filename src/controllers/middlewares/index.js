const swaggerJsDoc = require("swagger-jsdoc");
const swaggerUi = require("swagger-ui-express");
const express = require("express");
const helmet = require("helmet");
const path = require("path");
const passport = require("passport");
const session = require("express-session");
const controllersFolder = path.join(__dirname, "../routers/*.js");

console.log(controllersFolder);
const swaggerOptions = {
  swaggerDefinition: {
    swagger: "2.0",
    info: {
      title: "Delilah Resto",
      version: "1.0.0",
      description: "API para conectarse al Delilah Resto",
      contact: {
        email: "anrocampa@gmail.com",
      },
    },
    host: "andreacampanella.tk",
    basePath: "",
    schemes: ["https", "http"],
    // components: {
    securityDefinitions: {
      BearerAuth: {
        type: "apiKey",
        name: "token",
        scheme: "bearer",
        in: "header",
      },
      // }
    },
    security: [
      {
        BearerAuth: [],
      },
    ],
  },
  apis: [controllersFolder],
};
const swaggerDocs = swaggerJsDoc(swaggerOptions);

function prepareMiddlewares(server) {
  server.use(helmet());
  server.use("/api-docs", swaggerUi.serve, swaggerUi.setup(swaggerDocs));
  server.use(express.json());
  server.use(express.urlencoded({ extended: false }));
  server.use(
    session({
      resave: false,
      saveUninitialized: true,
      secret: "1234567890",
    })
  );
  server.use(passport.initialize());
  server.use(passport.session());
}

module.exports = {
  prepareMiddlewares,
};
