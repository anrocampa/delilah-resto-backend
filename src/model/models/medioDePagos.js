const { DataTypes } = require("sequelize");

function createModel(connection) {
  const MedioDePago = connection.define("Mediopago", {
    id: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true,
    },
    nombre: DataTypes.STRING,
    descripcion: DataTypes.STRING,
    icono: DataTypes.STRING,
  });

  return MedioDePago;
}

module.exports = {
  createModel,
};
