const { DataTypes } = require("sequelize");
const { PEDIDOS_STATUS } = require("../../constants");

function createModel(connection) {
  const Pedidos = connection.define("Pedido", {
    id: { type: DataTypes.INTEGER, autoIncrement: true, primaryKey: true },
    formaPago: DataTypes.INTEGER,
    direccion: DataTypes.STRING,
    fecha: {
      type: DataTypes.DATE,
      allowNull: false,
      defaultValue: DataTypes.NOW,
    },
    username: DataTypes.STRING,
    estado: {
      type: DataTypes.STRING,
      allowNull: false,
      defaultValue: PEDIDOS_STATUS.Pendiente,
    },
  });

  return Pedidos;
}
module.exports = {
  createModel,
};
