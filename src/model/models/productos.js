const { DataTypes } = require("sequelize");

function createModel(connection) {
  const Producto = connection.define("Producto", {
    id: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true,
    },
    nombre: DataTypes.STRING,
    precio: DataTypes.STRING,
    fotoUrl: DataTypes.STRING,
  });

  return Producto;
}

module.exports = {
  createModel,
};
