const { DataTypes } = require("sequelize");

function createModel(connection) {
  const PedidoProducto = connection.define("PedidoProducto", {
    cantidad: DataTypes.INTEGER,
  });

  return PedidoProducto;
}

module.exports = {
  createModel,
};
