const { DataTypes } = require("sequelize");

//class usuario extends Model {}

function createModel(connection) {
  const Usuario = connection.define("Usuario", {
    username: {
      type: DataTypes.STRING,
      primaryKey: true,
    },
    nombreApellido: DataTypes.STRING,
    email: {
      type: DataTypes.STRING,
      allowNull: false,
      unique: true,
    },
    direccion: DataTypes.STRING,
    telefono: DataTypes.STRING,
    clave: DataTypes.STRING,
    activo: { type: DataTypes.BOOLEAN, defaultValue: true },
    esAdmin: { type: DataTypes.BOOLEAN, defaultValue: false },
    id_facebook: { type: DataTypes.STRING, defaultValue: "" },
  });
  return Usuario;
}

module.exports = {
  createModel,
};
