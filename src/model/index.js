const Sequelize = require("sequelize");
const mysql = require("mysql2/promise");

const redis = require("async-redis");

const { createModel: createMedioDePagoModel } = require("./models/medioDePagos");
const { createModel: createUsuarioModel } = require("./models/usuarios");
const { createModel: createPedidoModel } = require("./models/pedidos");
const { createModel: createProductoModel } = require("./models/productos");
const { createModel: createPedidoProductoModel } = require("./models/pedidoproducto");

let models = {};

async function connect(host, port, username, password, database) {
  try {
    console.log("database connecting ....", host, port, username, password, database);
    const createDBconn = await mysql.createConnection({
      host: host,
      port: port,
      user: username,
      password: password,
    });
    console.log("database conectado....");
    await createDBconn.query(`CREATE DATABASE IF NOT EXISTS \`${database}\`;`);

    const connection = new Sequelize(database, username, password, {
      host: host,
      port: port,
      dialect: "mysql",
      logging: (msg) => console.log(msg),
    });

    /*{
    dialect: "sqlite",
    storage: "./database.sqlite",
  });*/

    models.MedioDePago = createMedioDePagoModel(connection);
    models.Pedido = createPedidoModel(connection);
    models.Usuario = createUsuarioModel(connection);
    models.Producto = createProductoModel(connection);
    models.PedidoProducto = createPedidoProductoModel(connection);

    models.Pedido.belongsToMany(models.Producto, {
      through: models.PedidoProducto,
    });
    //models.Producto.belongsToMany(models.Pedido, { through: models.PedidoProducto });

    await connection.authenticate();
    await connection.sync();
    console.log("Database Connection has been established successfully.");
  } catch (error) {
    console.error("Unable to connect to the database:", error);
  }
}

async function redisConnect(REDIS_HOST, REDIS_PORT) {
  try {
    const client = await redis.createClient({
      host: REDIS_HOST,
      port: REDIS_PORT,
    });
    client.on("error", (error) => {
      console.error("Unable to connect to Redis Server:", error);
    });

    models.Redis = client;
    console.log("Redis server connection has been established successfully.");
  } catch (error) {
    console.error("Unable to connect to Redis Server:", error);
  }
}

function getModel(name) {
  if (!models[name]) {
    global.console.log("No existe");
    return null;
  }
  return models[name];
}

module.exports = {
  connect,
  getModel,
  redisConnect,
};
