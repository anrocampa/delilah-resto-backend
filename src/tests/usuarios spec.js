const request = require("supertest");
const sinon = require("sinon");

const { makeServer } = require("../index");
const db = require("../model");

const server = makeServer();
const usuarioData = {
  nombreApellido: "andrea campanella",
  email: "andrea@andrea.com",
  direccion: "uy",
  telefono: "+1 234567890",
  username: "Andrea",
  clave: "claveTest12344321",
};

describe("POST /usuarios", () => {
  it("should post usuarios - Datos invalidos", (done) => {
    request(server)
      .post("/usuarios")
      .expect("Content-Type", /json/)
      .expect(403)
      .end(function (err, res) {
        if ((res.body.mensaje = "Datos inválidos")) {
          done();
        }
        if (err) throw err;
      });
  });

  it("should post usuarios - Nuevo Usuario", (done) => {
    sandbox = sinon.createSandbox();
    const ModeloFalso = {
      findAll() {
        return Promise.resolve({});
      },
      create() {
        return Promise.resolve({});
      },
    };
    sandbox.stub(db, "getModel").returns(ModeloFalso);

    request(server)
      .post("/usuarios")
      .send(usuarioData)
      .expect("Content-Type", /json/)
      .expect(201)
      .end(function (err, res) {
        if (res.body.mensaje == "Usuario creado") {
          done();
        } else if (err) throw err;
        sandbox.restore();
      });
  });

  it("should post usuarios - Usuario Duplicado", (done) => {
    sandbox = sinon.createSandbox();
    const ModeloFalso = {
      findAll() {
        return Promise.resolve([{ id: 1 }]); // usuario duplicado
      },
    };
    sandbox.stub(db, "getModel").returns(ModeloFalso);

    request(server)
      .post("/usuarios")
      .send(usuarioData)
      .expect("Content-Type", /json/)
      .expect(403)
      .end(function (err, res) {
        if (res.body.mensaje == "Usuario duplicado") {
          done();
        } else if (err) throw err;
        sandbox.restore();
      });
  });
});
