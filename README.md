# Delilah Resto

Backend server de Delilah Resto

# Ejecución en AWS (Sprint 3)

## Conectar al servidor

- Ingresar a la url https://aws.amazon.com/ y presionar Sign IN
- Seleccionar IAM User
- Ingresar Account Id: `ami_account_id` (están en archivo cred_ami.txt )
- Ingresar usuario: `ami_user`
- Ingresar password: `ami_password`

## Sitios

- Url dominio propio: https://andreacampanella.tk
- Url Swagger : https://andreacampanella.tk/api-docs

## Probar API

- Ingresando a postman
- Importar archivo `Delilah Restoran.postman_AWS.json`

## Agregar rule para Acceder por SSH

- En el buscador de la barra superior ingresar EC2, seleccionarlo
- Acceder al menú Security Groups
- Buscar en la lista y hacer click sobre `InstanciaSprint3`
- Precionar botón Edit inbound rules
- Precionar botón Add rule
- Seleccionar Type SSH
- Seleccionar Source My IP y luego presionar Save Rules

## Acceder por SSH a Instancias

- En el buscador de la barra superior ingresar EC2, seleccionarlo
- Acceder al menú Instances
- Hacer click en la instancia
- Presionar el botón Connect, en la solapa SSH muestra el instructivo para conectar
- Utilizar archivo pem (techreview.pem)

# Ejecución en Desarrollo (Sprint 2)

## Pre requisitos

- MySql server instalado
- Disponer de un usuario en Mysql con capacidad para conectarse/select/insert/delete/update desde el servidor que ejectuaremos nuestro backend
- Servidor REDIS instalado para ser utilizado como caché
- Node

## Instalación

- Clonar el repositorio, e instalar las dependencias

```console
git clone https://gitlab.com/anrocampa/delilah-resto-backend

cd delilah-resto-backend

npm install
```

- Modificar variables de entorno al ambiente de ejecución,

  - editar el archivo .env y agregar o modificar las variables de entorno
  - todas las variables de entorno son obligatorias

```console
vi .env
```

### Ejemplo de .env

```console
DB_USERNAME= usuarioMysql               // usuario de Mysql
DB_PASSWORD= passwordDelUsuarioMysql    // clave del usuario Mysql
DB_NAME= resto_00                       // nombre de la BD que se creará en Mysql
DB_HOST= 127.0.0.1                      // host del servidor Mysql
DB_PORT= 3306                           // puerto de escucha de Mysql
DB_ROOT_PASSWORD=rootsecret             // clave del usuario Mysql Root (necesario para inicializar con docker-compose)

REDIS_HOST = 127.0.0.1                  // host del servidor cache Redis
REDIS_PORT = 6379                       // puerto de escucha de Redis

JWT_PASS = La4Pass453del536JWT          // semilla para generar el JWT
SERVER_PORT = 3000                      // puerto donde escuchará nuestro backend server

FACEBOOK_CLIENT_ID=xxxxxxxxxxxxx
FACEBOOK_CLIENT_SECRET=yyyyyyyyyyyyyyy
FACEBOOK_CALLBACK_URL=http://localhost:3000/auth/facebook/callback

MP_KEY=xxxxxxxxxxxxx
MP_TOKEN=yyyyyyyyyyyyyyy
MP_REDIRECT_SERVER_URL=http://localhost:3000
```

## Sprint 4

Se realizaron todos los puntos obligatorios (login Facebook, pago MercadoPago) y el punto adicional de docker-compose


### Run - docker

    - estando en el directorio `delilah-resto-backend`,
    - ejecutar

```console
docker build . -t sprint4
docker run --name sprint4container -p 3000:3000  --env-file .env -d sprint4
```


### Run as docker-compose

    - estando en el directorio `delilah-resto-backend`,
    - ejecutar

```console
docker-compose --env-file .env up
```

### Probar Integracion de Login Facebook (único servicio de Auth implementado)

    - como no tenemos UI, nosotros debemos de simular las acciones como si fuésemos la aplicacion cliente
    - desde un browser, abrir la url `https://andreacampanella.tk/auth/facebook`
    - si el login es exitoso obtenemos como respuesta el token de acceso que
      después debemos de usar en los llamados a los endpoints con una aplicacion como Postman.
    - por ejemplo para crear un pedido y despues pagarlo con MercadoPago


### Cuentas de prueba para Auth de facebook

    - user: lvunmctimi_1672264958@tfbnw.net
    - secret: Test0987

    - user: kibpqykbly_1672264958@tfbnw.net
    - secret: Test0987

### Probar pago con MercadoPago (única pasarela de pago implementada)

    - al igual que con el login de Facebook, debemos de simular las acciones de la aplicacion cliente
    - desde postman, ejecutar el endpoint GET a `https://andreacampanella.tk/pagar/{:id_pedido} (con su correspondiente token auth de usuario)
    - si todo es correcto, obtenemos como respuesta la url para finalizar el pago
    - abrir dicha url en un browser para finalizar el pago

### Recordar:

    - para pagar el pedido éste debe de estar "Confirmado" (solo Admins pueden cambiar el estado)
    - el token de auth se utiliza en el 'Header' del request, key 'token'

### Pasos genericos para crear y pagar pedido

    - una vez logeado (usuario), crear un pedido (con token de usuario)
        ('Postman', 'Pedidos', 'CREAR pedido')

    - obtener token de Login como admin
        ('Postman', 'Usuarios', 'login admin')

    - confirmar pedido (con token de admin)
        ('Postman', 'Pedidos', 'CONFIRMAR pedido (admin)')
        usar codigo de pedido creado anteriormente

    - pagar pedido (con token de usuario)
        ('Postman', 'Pedidos', 'PAGAR pedido')
        usar codigo de pedido creado anteriormente

### Tarjetas para SandBox de MercadoPago

#### Mastercard

- Nro: 5031755734530604
- CVV: 123
- Exp: 11/25

#### Visa

- Nro: 4509953566233704
- CVV: 123
- Exp: 11/25

## Usage

### Run backend server

```console
npm run dev
```

### Run tests

```console
npm run test
```

### Endpoints en Postman

Se adjunta colección de postman como guía para la ejecución de las llamadas a los endpoints desde la aplicación Postman

archivo adjunto:
`Delilah Restoran.postman_collection.json`

### Documentación en Swagger:

http://https://127.0.0.1:3000/api-docs/
